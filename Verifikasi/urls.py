from django.contrib.auth import logout
from app.viewset_api import data_all, data_detail , tambah_data , RegisterAPI , LoginAPI
from django.contrib import admin
from django.urls import path , include
from app.views import home , tambahdata , dataview
from rest_framework.urlpatterns import format_suffix_patterns
from knox.views import LogoutAllView

urlpatterns = [
    path('api/logout' , LogoutAllView.as_view()),
    path('api/login', LoginAPI.as_view()),
    path('api/register' , RegisterAPI.as_view()),
    path('api/tambah' , tambah_data),
    path('api/data/' , data_all),
    path('api/data/<int:id>' , data_detail),
    path('admin/', admin.site.urls),
    path('' , home ),
    path('tambah-data/' , tambahdata , name='tambah-data'),
    path('list-data' , dataview , name='list-data')
]
urlpatterns = format_suffix_patterns(urlpatterns)